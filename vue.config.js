const CompressionWebpackPlugin = require('compression-webpack-plugin');

module.exports = {
  publicPath: './', // 部署URL支持相对路径
  productionSourceMap: false,
  chainWebpack: (config) => {
    /* 添加分析工具 */
    if (process.env.NODE_ENV === 'production') {
      config
        .plugin('webpack-bundle-analyzer')
        .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
        .end()
      config.plugins.delete('prefetch')
    } 
  },
  configureWebpack: () => {
    if (process.env.NODE_ENV !== 'production')
      return {}

    let config = {
      // 拆包
      optimization: {
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 500000, // 依赖包超过xx bit将被单独打包
          automaticNameDelimiter: '-',
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                return `chunk.${packageName.replace('@', '')}`;
              },
              priority: 10
            }
          }
        }
      }
    }

    // gzip 压缩
    config.plugins = [
      new CompressionWebpackPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /.(js|css|svg|woff|ttf|json)$/,
        threshold: 10240, // 过滤大小 > xx bit
        minRatio: 0.8 // 过滤压缩率 < 0.8
      })
    ]

    return config
  },

};